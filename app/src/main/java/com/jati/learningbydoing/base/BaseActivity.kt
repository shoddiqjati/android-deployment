package com.jati.learningbydoing.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.jati.learningbydoing.R
import com.jati.learningbydoing.di.component.AppComponent
import kotlinx.android.synthetic.main.toolbar.*

/**
 * Created by jati on 10/03/18.
 */
abstract class BaseActivity<T> : AppCompatActivity() {
    val appComponent: AppComponent? by lazy {
        (application as BaseApp).appComponent
    }

    var presenter: T? = null

    fun setupToolbarTitle(toolbar: Toolbar, title: Int = R.string.empty_string, icon: Int = R.drawable.ic_back) {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeButtonEnabled(true)
            it.setHomeAsUpIndicator(icon)
        }
        toolbar_title.setText(title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onSetupLayout()
        onViewReady()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.let {
            (it as BasePresenter).clearDisposable()
        }
    }

    protected abstract fun onSetupLayout()
    protected abstract fun onViewReady()
}