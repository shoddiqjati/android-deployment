package com.jati.learningbydoing.base

import android.app.Application
import com.jati.learningbydoing.di.component.AppComponent
import com.jati.learningbydoing.di.component.DaggerAppComponent
import com.jati.learningbydoing.di.module.NetworkModule
import com.jati.learningbydoing.di.module.UtilModule

/**
 * Created by jati on 10/03/18.
 */
class BaseApp : Application() {
    val appComponent: AppComponent? by lazy(DaggerAppComponent
            .builder()
            .networkModule(NetworkModule())
            .utilModule(UtilModule(this))::build)
}