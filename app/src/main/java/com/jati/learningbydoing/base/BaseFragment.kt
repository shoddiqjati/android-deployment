package com.jati.learningbydoing.base

import android.support.v4.app.Fragment

/**
 * Created by jati on 10/03/18.
 */
abstract class BaseFragment<T> : Fragment() {
    protected var presenter: T? = null
    protected val appComponent by lazy {
        (activity?.application as BaseApp).appComponent
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter?.let {
            (it as BasePresenter).clearDisposable()
        }
    }
}