package com.jati.learningbydoing.base

import io.reactivex.disposables.CompositeDisposable

/**
 * Created by jati on 10/03/18.
 */
abstract class BasePresenter {
    protected val compositeDisposable = CompositeDisposable()

    fun clearDisposable() {
        compositeDisposable.clear()
    }
}
