package com.jati.learningbydoing.di.component

import com.jati.learningbydoing.di.module.NetworkModule
import com.jati.learningbydoing.di.module.UtilModule
import com.jati.learningbydoing.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by jati on 10/03/18.
 */
@Singleton
@Component(modules = arrayOf(NetworkModule::class, UtilModule::class))
interface AppComponent {
    fun inject(mainActivity: MainActivity)
}