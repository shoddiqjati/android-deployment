package com.jati.learningbydoing.di.module

import android.content.Context
import com.jati.learningbydoing.utils.SharedPreferenceUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by jati on 10/03/18.
 */
@Module
class UtilModule(val context: Context) {
    @Provides
    @Singleton
    fun provideSharedPreferenceUtil() = SharedPreferenceUtils(context)
}