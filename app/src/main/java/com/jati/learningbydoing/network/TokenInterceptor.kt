package com.jati.learningbydoing.network

import com.jati.learningbydoing.utils.SharedPreferenceUtils
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by jati on 10/03/18.
 */
class TokenInterceptor(private val sharedPref: SharedPreferenceUtils) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val builder = request.newBuilder()
        builder.addHeader("Authorization", "bearer ${sharedPref.getString(SharedPreferenceUtils.PREF_TOKEN)}")
        return chain.proceed(builder.build())
    }
}