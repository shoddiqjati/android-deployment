package com.jati.learningbydoing.ui

import com.jati.learningbydoing.R
import com.jati.learningbydoing.base.BaseActivity

class MainActivity : BaseActivity<MainPresenter>(), MainView {

    override fun onSetupLayout() {
        setContentView(R.layout.activity_main)
        appComponent?.inject(this)
        presenter = MainPresenter()
    }

    override fun onViewReady() {}

}