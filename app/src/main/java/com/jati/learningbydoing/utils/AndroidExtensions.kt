package com.jati.learningbydoing.utils

import android.content.Context
import android.view.View
import android.widget.Toast

/**
 * Created by jati on 10/03/18.
 */
fun Context.toast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    val toastBuilder = Toast.makeText(this, message, duration)
    if (toastBuilder != null) {
        toastBuilder.cancel()
        toastBuilder.show()
    } else {
        toastBuilder?.show()
    }
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

