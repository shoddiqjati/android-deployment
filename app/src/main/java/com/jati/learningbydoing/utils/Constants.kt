package com.jati.learningbydoing.utils

/**
 * Created by jati on 10/03/18.
 */
class Constants {
    object SharedPreferences {
        val SHARED_PREFERENCES_NAME = "learning_pref"
    }
}