package com.jati.learningbydoing.utils

import android.content.Context

/**
 * Created by jati on 10/03/18.
 */
class SharedPreferenceUtils(context: Context) {

    companion object {
        val PREF_TOKEN = "pref_token"
    }

    private val sharedPreferenceUtil = context.getSharedPreferences(Constants.SharedPreferences.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)

    fun setBoolean(key: String, value: Boolean) {
        sharedPreferenceUtil.edit().putBoolean(key, value).apply()
    }

    fun setString(key: String, value: String) {
        sharedPreferenceUtil.edit().putString(key, value).apply()
    }

    fun setInt(key: String, value: Int) {
        sharedPreferenceUtil.edit().putInt(key, value).apply()
    }

    fun setLong(key: String, value: Long) {
        sharedPreferenceUtil.edit().putLong(key, value).apply()
    }

    fun getBoolean(key: String) = sharedPreferenceUtil.getBoolean(key, false)

    fun getString(key: String) = sharedPreferenceUtil.getString(key, "")

    fun getInt(key: String) = sharedPreferenceUtil.getInt(key, 0)

    fun getLong(key: String) = sharedPreferenceUtil.getLong(key, 0)
}